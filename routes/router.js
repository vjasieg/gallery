var express = require('express');
var app = express();
const GalleryService = require("../services/gallery/GalleryService")

app.get("/album/get", GalleryService.getAll)

module.exports = app;